const express = require("express");
const app = express();

const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
    user:"root",
    host:"localhost",
    password:"niranjan",
    database:"employeedetails"
});


app.post("/create",(req,res)=>{
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const phonenumber = req.body.phonenumber;
    const jobtitle = req.body.jobtitle;


db.query("INSERT INTO employees(firstname, lastname, email, phonenumber, jobtitle) VALUES(?,?,?,?,?)",
[firstname, lastname, email, phonenumber, jobtitle],
(err,result)=>{
    if (err){
        console.log(err);
    }else{
        res.send("Values Inserted");
    }
} );
});

app.get("/employees",(req,res)=>{
    
    db.query("SELECT * FROM employees",(err,result)=>{
       // console.log('Hi');
        if (err){
            console.log(err);
        }/*else{
            console.log(result);
        }*/res.json(result)
    });
});

app.listen(3001, ()=>{
    console.log("running on port 3001");
});