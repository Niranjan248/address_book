import React, { Component } from 'react'
import axios from 'axios'

export class ContactDetails extends Component {
    constructor(props) {
        super(props)


        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            phonenumber: '',
            jobtitle: '',
            employeeList: undefined

        }
    }
    changeHandlerFirstname(event) {
        this.setState({
            firstname: event.target.value
        })
    }
    changeHandlerLastname(event) {
        this.setState({
            lastname: event.target.value
        })
    }
    changeHandlerEmail(event) {
        this.setState({
            email: event.target.value
        })
    }
    changeHandlerPhonenumber(event) {
        this.setState({
            phonenumber: event.target.value
        })
    }
    changeHandlerJobtitle(event) {
        this.setState({
            jobtitle: event.target.value
        })
    }
    save() {
        window.alert(`The contact ${this.state.firstname} has been saved`);
        //console.log(this.state.firstname)
        //console.log(this.state.email )
        // event.preventDefault()
        axios
            .post("http://localhost:3001/create", {
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                email: this.state.email,
                phonenumber: this.state.phonenumber,
                jobtitle: this.state.jobtitle
            })
            .then(response => {
                console.log("Success")
            })
        // console.log("submitted")

    }


    getContact() {
        console.log("String")
        axios.get("http://localhost:3001/employees")
            .then(response => {
                this.setState({ employeeList: response.data })
            })
    }
    
    render() {
        //const { firstname, lastname, email, phonenumber, jobtitle } = this.state
        return (
            <div className="Contact">
                { console.log(this.state)}
                <h1 >CONTACT DETAILS</h1>
                
                    <label>First Name: </label>
                    <input type="text"  onChange={this.changeHandlerFirstname.bind(this)} /><br />
                    <label>Last Name: </label>
                    <input type="text"  onChange={this.changeHandlerLastname.bind(this)} /><br />
                    <label>Email: </label>
                    <input type="email"  onChange={this.changeHandlerEmail.bind(this)} /><br />
                    <label>Phone Number: </label>
                    <input type="number"  onChange={this.changeHandlerPhonenumber.bind(this)} /><br />
                    <label>Job Title: </label>
                    <input type="text"  onChange={this.changeHandlerJobtitle.bind(this)} /><br />
                    <button onClick={this.save.bind(this)}>Submit</button><br />
                    <button onClick={this.getContact.bind(this)}>Show Contacts</button>
                    {this.state.employeeList !== undefined && this.state.employeeList.map(employee => {
                        return <div key={employee.id}>{employee.firstname}</div>
                    })}
                




            </div>
        )
    }
}

export default ContactDetails
