
import React from "react"
import './App.css';
import ContactDetails from './Components/ContactDetails'

function App() {
  
  return (
    <div className="App">
      
      <ContactDetails />
    </div>
  );
}

export default App;
